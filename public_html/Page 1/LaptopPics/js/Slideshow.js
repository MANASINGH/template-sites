//The JSON global JSON object
var jsonObj;
var counter = 0;
var playing = false;
var willStop = 0;

$(function(){

   // When the page starts up, this script will execute
   
   $.getJSON("Slideshow_Data.json", function(data){
	   jsonObj = data; //First read the JSON 
	   $("#title").html(jsonObj.title); //Set the title on the webpage to the title that the JSON contains
	   $("#image").attr("src", "img/" + jsonObj.slides[0].image_file_name);
	   $("#caption").html(jsonObj.slides[0].caption);
	   $("#play").attr("src", "img/" + "play.png");
	   $("#prev").attr("src", "img/" + "prev.png");
	   $("#next").attr("src", "img/" + "next.png");
	   $("p").html(jsonObj.slides[0].image_file_name);
   });
   
   

});

function playShow(){
	playing = true;
	$("#play").attr("src", "img/" + "pause.png");
	$("#playDiv").attr("onclick", "pauseShow()");
	willStop = setInterval(function(){
		nextSlide();
	}, 3000);
}

function nextSlide(){
	counter = counter + 1;
	if(counter > jsonObj.slides.length - 1){
		counter = 0;
	}
	$("#image").attr("src", "img/" + jsonObj.slides[counter].image_file_name);
	$("#caption").html(jsonObj.slides[counter].caption);
	console.log(jsonObj.slides[counter].image_file_name);
}

function prevSlide(){
	counter = counter - 1;
	if(counter < 0){
		counter = jsonObj.slides.length - 1;
	}
	$("#image").attr("src", "img/" + jsonObj.slides[counter].image_file_name);
	$("#caption").html(jsonObj.slides[counter].caption);
}

function pauseShow(){
	playing = false;
	$("#play").attr("src", "img/" + "play.png");
	$("#playDiv").attr("onclick", "playShow()");
	clearInterval(willStop);
}

