$(function(){

   // When the page starts up, this script will execute
   
   $.getJSON("Test.json", function(elementsToAdd){
	   jsonObj = elementsToAdd; //First read the JSON 
	   for(var i = 0; i < elementsToAdd.webpageContent.length; i++){
               var currentType = elementsToAdd.webpageContent[i].type;
               
               switch(currentType){
                   case "h1": addHeader(elementsToAdd.webpageContent[i].data);
                   break;
                   
                   case "p": addParagraph(elementsToAdd.webpageContent[i].data);
                   break;
                   
                   case "ul": addUnorderedList(elementsToAdd.webpageContent[i].data);
                   break;
                   
                   case "img": addImage(elementsToAdd.webpageContent[i].data);
                   break;
                   
                   case "a": addLink(elementsToAdd.webpageContent[i].data);
                   break;
                   
                   case "video": addVideo(elementsToAdd.webpageContent[i].data);
                   break;
                   
                   case "slideshow": addSlideShow(elementsToAdd.webpageContent[i].data);
                   break;
                   
                   default: continue;
                   
               }
           }
   });
   
   function addHeader(data){
       $("#content").append("<h1>" + data + "</h1>");
       $("#content").append("</br>");
   }
   
   function addParagraph(data){
       $("#content").append("<p>" + data + "</p>");
       $("#content").append("</br>");
   }
   
   function addUnorderedList(data){
       var ulTag = "<ul>";
       for(var i = 0; i < data.length; i++){
           ulTag += ("<li>" + data[i] + "</li>");
       }
       ulTag += "</li>";
       $("#content").append(ulTag);
       $("#content").append("</br>");
   }
   
   function addImage(data){
       var img = $("<img>");
       img.attr("src", data.src);
       img.width(data.width);
       img.height(data.height);
       $("#content").append(img);
       $("#content").append("</br>");
   }

    function addLink(data){
        var link = $("<a>" + data.text + "</a>");
        link.attr("href", data.url);
        $("#content").append(link);
        $("#content").append("</br>");
    }
    
    function addVideo(data){
        var video = $("<video></video>");
        video.attr("width", data.width);
        video.attr("controls", "");
        var source1 = $("<source>");
        source1.attr("src", data.src);
        source1.attr("type", "video/mp4");
        video.append(source1);
        $("#content").append(video);
        $("#content").append("</br>");
    }
    
    function addSlideShow(data){
        var slideshow = $("<iframe></iframe>");
        slideshow.attr("width", "800");
        slideshow.attr("height", "850");
        slideshow.attr("src", data + "/index.html");
        $("#content").append(slideshow);
        $("#content").append("</br>");
        
        
    }
});


